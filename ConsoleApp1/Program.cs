﻿using System;
using System.IO;

namespace ConsoleApp1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Не вказано шлях до каталогу.");
                return;
            }

            string directoryPath = args[0];
            Console.WriteLine("Режим роботи з усіма файлами каталогу:");
            CountFilesInDirectory(directoryPath);

            Console.WriteLine();

            Console.WriteLine("Режим роботи з вказаними файлами за шаблоном (*.exe):");
            string filePattern = "*.exe";
            CountFilesByPattern(directoryPath, filePattern);

            Console.ReadLine();
        }

        private static void CountFilesInDirectory(string directoryPath)
        {
            try
            {
                int fileCount = 0;
                int hiddenFileCount = 0;
                int readOnlyFileCount = 0;
                int archiveFileCount = 0;

                DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
                FileInfo[] files = directoryInfo.GetFiles("*", SearchOption.AllDirectories);

                foreach (FileInfo file in files)
                {
                    fileCount++;

                    if ((file.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                        hiddenFileCount++;

                    if ((file.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        readOnlyFileCount++;

                    if ((file.Attributes & FileAttributes.Archive) == FileAttributes.Archive)
                        archiveFileCount++;
                }

                Console.WriteLine("Кількість файлів: " + fileCount);
                Console.WriteLine("Кількість прихованих файлів: " + hiddenFileCount);
                Console.WriteLine("Кількість файлів тільки для читання: " + readOnlyFileCount);
                Console.WriteLine("Кількість архівних файлів: " + archiveFileCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Помилка: " + ex.Message);
            }
        }

        private static void CountFilesByPattern(string directoryPath, string filePattern)
        {
            try
            {
                int fileCount = 0;
                int hiddenFileCount = 0;
                int readOnlyFileCount = 0;
                int archiveFileCount = 0;

                DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
                FileInfo[] files = directoryInfo.GetFiles(filePattern, SearchOption.AllDirectories);

                foreach (FileInfo file in files)
                {
                    fileCount++;

                    if ((file.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                        hiddenFileCount++;

                    if ((file.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        readOnlyFileCount++;

                    if ((file.Attributes & FileAttributes.Archive) == FileAttributes.Archive)
                        archiveFileCount++;
                }

                Console.WriteLine("Кількість файлів за шаблоном " + filePattern + ": " + fileCount);
                Console.WriteLine("Кількість прихованих файлів за шаблоном " + filePattern + ": " + hiddenFileCount);
                Console.WriteLine("Кількість файлів тільки для читання за шаблоном " + filePattern + ": " + readOnlyFileCount);
                Console.WriteLine("Кількість архівних файлів за шаблоном " + filePattern + ": " + archiveFileCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Помилка: " + ex.Message);
            }
        }
    }
}